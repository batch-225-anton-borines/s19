console.log("Hello, World!");

// Conditional Statements - allows us to control the flow of our program


// [SECTION] if, else if, else 

/*
Syntax:

	if(condition){
		statement
	};
*/

// if Statement

let numA = 5;

if(numA < 3) {
	console.log('Hello');
}

let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York City!");
}

// else if Clause

/*
	- Executes a statement if previous conditions are false and if the specified condition is true
	- The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program
*/


numA = 5;
let numB = 1;

if(numA > 3) {
	console.log("Hello");
} else if (numB > 0){
	console.log("World");
}

// else if() statement no longer ran becaue the if statement was able to run and the evaluation of the whole statement stopped there

// else if in string

city = "Tokyo";

if(city == "New York") {
	console.log("Welcome to New York City!");
} else if(city == "Tokyo") {
	console.log("Welcome to Tokyo, Japan!");
}

// else statement 
/*
	- executes a statement if all other conditions are false
	- the "else" statement is optional and can be used to capture any other result to change the flow the program
*/

let numC = -5;
let numD = 7;

if(numC > 0) {
	console.log("Hello");
} else if(numD === 0) {
	console.log("World");
} else {
	console.log("Again");
}

// if, else, and else statements with function

function determineTyphoonIntensity (windSpeed) {
	if (windSpeed < 30) {
		return "Not a Typhoon yet.";
	} 
	else if (windSpeed < 62) {
		return "Tropical Depression detected.";
	}
	else if (windSpeed < 88) {
		return "Tropical Storm detected";
	}
	else if (windSpeed < 117) {
		return "Severe Tropical Storm detected.";
	}
	else {
		return "Typhoon detected!"
	}
}
let message = determineTyphoonIntensity(87);

if (message == "Tropical Storm detected") {
	console.warn(message);
}

// [SECTION] Conditional (Ternary) Operator

/*
	- The Conditional (Ternary) Operator takes in three operands:
	1. condition
	2. expression to execute if the condition is true
	3. expression to execute if the condition is false

	- Syntax
		(expression) ? ifTrue : ifFalse
*/

// Single statement execution

let ternaryResult = (1 < 18) ? true : false;

console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple Statement execution

let name;

function isOfLegalAge() {
	return "You are of the legal age"
}

function isUnderAge(){
	return "You are under the age limit";
}

// The "parseInt" function converts the input received into a number data type

name = prompt("What is your name?");
let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >=18) ? isOfLegalAge() : isUnderAge();
console.log(legalAge + ", " + name);

// [SECTION] Switch Statement

/*
	- the switch statement evaluates an expression and matches the expression's value to a case clause.
	- the switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case. 
	- can be used as an alternative to an if, else if, and else statement where the data to be used in the condition is of an expected output
	- the ".toLowerCase()" method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case condition if the user inputs capital or uppercase letters 
*/

/*
	switch (expression) {
		case value:
			statement;
			break;
		default: statement;
	}
*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}